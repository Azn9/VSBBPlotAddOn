# PlotSquared VoxelSniper and BetterBrushed AddOn

This project was created to be used on a  1.14.X minecraft server with PlotSquared, Voxel Sniper (and BetterBrushes (optional)). This plugin authorize the use of voxel sniper's commands, items etc. only on your own plot.

[Download the plugin](https://gitlab.com/Azn9/VSBBPlotAddOn/-/raw/master/build/libs/PlotSquarredAddOn-0.0.1-SNAPSHOT.jar)