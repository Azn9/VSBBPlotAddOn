package dev.azn9.PSAO;

import com.github.intellectualsites.plotsquared.api.PlotAPI;
import com.github.intellectualsites.plotsquared.bukkit.events.PlayerEnterPlotEvent;
import com.github.intellectualsites.plotsquared.bukkit.events.PlayerLeavePlotEvent;
import com.sk89q.worldedit.math.BlockVector3;
import com.thevoxelbox.voxelsniper.VoxelSniper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommandYamlParser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PlotAddOn extends JavaPlugin implements Listener, CommandExecutor {

    /////////////////////////////////////////
    //               VARIABLES             //
    /////////////////////////////////////////

    private static final List<String> UNAUTHORIZED_COMMANDS = new ArrayList<>();
    private static final PlotAPI      PLOT_API              = new PlotAPI();

    /////////////////////////////////////////
    //             INITIALIZATION          //
    /////////////////////////////////////////

    @Override
    public void onEnable() {
        Logger logger = Bukkit.getLogger();

        logger.info("Initializing plugin...");

        Optional.ofNullable(Bukkit.getPluginManager().getPlugin("VoxelSniper")).ifPresent(plugin ->
                PluginCommandYamlParser.parse(plugin).forEach(command -> {
                            UNAUTHORIZED_COMMANDS.add(command.getName());
                            UNAUTHORIZED_COMMANDS.addAll(command.getAliases());
                        }
                ));

        Optional.ofNullable(Bukkit.getPluginManager().getPlugin("BetterBrushes")).ifPresent(plugin ->
                PluginCommandYamlParser.parse(plugin).forEach(command -> {
                            UNAUTHORIZED_COMMANDS.add(command.getName());
                            UNAUTHORIZED_COMMANDS.addAll(command.getAliases());
                        }
                ));

        Bukkit.getPluginManager().registerEvents(this, this);

        logger.info("Plugin initialized !");
    }


    /////////////////////////////////////////
    //                EVENTS               //
    /////////////////////////////////////////

    @EventHandler
    public void onPlayerPreProcessCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();

        if (!UNAUTHORIZED_COMMANDS.contains((event.getMessage().contains(" ") ? event.getMessage().split(" ")[0] : event.getMessage()).replaceFirst("/", "")))
            return;

        if (!VoxelSniper.getInstance().getSniperManager().getSniperForPlayer(event.getPlayer()).isEnabled()) {
            player.sendMessage("§cVous n'êtes pas autorisé à utiliser cette commande ici !");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlotEnter(PlayerEnterPlotEvent event) {
        if (!event.getPlayer().hasPermission("voxelsniper.sniper"))
            return;

        if (event.getPlot().isOwner(event.getPlayer().getUniqueId()) || event.getPlot().isAdded(event.getPlayer().getUniqueId()))
            VoxelSniper.getInstance().getSniperManager().getSniperForPlayer(event.getPlayer()).setEnabled(true);
    }

    @EventHandler
    public void onPlotLeave(PlayerLeavePlotEvent event) {
        if (event.getPlayer().hasPermission("voxelsniper.sniper"))
            VoxelSniper.getInstance().getSniperManager().getSniperForPlayer(event.getPlayer()).setEnabled(false);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.PHYSICAL &&
                (event.getPlayer().getInventory().getItemInMainHand().getType() == Material.ARROW
                        || event.getPlayer().getInventory().getItemInMainHand().getType() == Material.GUNPOWDER
                        || event.getPlayer().getInventory().getItemInOffHand().getType() == Material.ARROW
                        || event.getPlayer().getInventory().getItemInOffHand().getType() == Material.GUNPOWDER
                )
        ) {

            if (VoxelSniper.getInstance().getSniperManager().getSniperForPlayer(event.getPlayer()).isEnabled())
                return;

            final boolean[] found = {false};

            Optional.of(event.getPlayer().getTargetBlock(new HashSet<>(Collections.singleton(Material.AIR)), 255)).ifPresent(block -> {
                if (block != null && block.getType() != Material.AIR) {
                    BlockVector3 position = BlockVector3.at(block.getX(), block.getY(), block.getZ());

                    PLOT_API.getPlotAreas(event.getPlayer().getWorld().getName()).forEach(plotArea -> plotArea.getPlots().forEach(plot -> {
                        if (found[0])
                            return;

                        if (plot.getPlayersInPlot().stream().anyMatch(plotPlayer -> plotPlayer.getUUID().equals(event.getPlayer().getUniqueId()))) {
                            plot.getRegions().forEach(cuboidRegion -> {
                                if (cuboidRegion.contains(position)) {
                                    found[0] = !plot.isOwner(event.getPlayer().getUniqueId()) && !plot.isAdded(event.getPlayer().getUniqueId());
                                }
                            });
                        }
                    }));

                    if (!found[0]) {
                        event.getPlayer().sendMessage("§cVous n'êtes pas autorisé à modifier les plots d'autres personnes !");
                        VoxelSniper.getInstance().getSniperManager().getSniperForPlayer(event.getPlayer()).setEnabled(false);
                        Bukkit.getScheduler().runTaskLaterAsynchronously(this, () -> VoxelSniper.getInstance().getSniperManager().getSniperForPlayer(event.getPlayer()).setEnabled(true), 10L);
                    }
                }
            });
        }
    }
}
